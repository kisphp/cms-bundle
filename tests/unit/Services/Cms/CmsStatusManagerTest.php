<?php

namespace Tests\Unit\CmsBundle\Services\Cms;

use Codeception\Test\Unit;
use Doctrine\ORM\EntityManagerInterface;
use Kisphp\CmsBundle\Model\CmsLayoutWidgetModel;
use Kisphp\CmsBundle\Services\Cms\CmsStatusManager;

/**
 * @group service
 */
class CmsStatusManagerTest extends Unit
{
    public function test_activate_witgets_by_interval()
    {
        /** @var EntityManagerInterface $em */
        $em = $this->getModule('Doctrine2')->em;

        $model = new CmsLayoutWidgetModel($em);

        $csm = new CmsStatusManager($model, 'Europe/Berlin');

        $this->assertGreaterThanOrEqual(0, $csm->activateWidgetsByInterval());
    }

    public function test_inactivate_witgets_by_interval()
    {
        /** @var EntityManagerInterface $em */
        $em = $this->getModule('Doctrine2')->em;

        $model = new CmsLayoutWidgetModel($em);

        $csm = new CmsStatusManager($model, 'Europe/Berlin');

        $this->assertGreaterThanOrEqual(0, $csm->deactivateWidgetsByInterval());
    }
}
