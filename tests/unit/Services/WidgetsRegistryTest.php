<?php

namespace Tests\Unit\CmsBundle\Services;

use Codeception\Test\Unit;
use Codeception\Util\Stub;
use Doctrine\ORM\EntityManagerInterface;
use Kisphp\CmsBundle\Entity\CmsLayoutWidget;
use Kisphp\CmsBundle\Model\CmsLayoutWidgetModel;
use Kisphp\CmsBundle\Services\ExampleWidgetsRegistry;
use Kisphp\Entity\KisphpEntityInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\FormBuilderInterface;

class WidgetsRegistryTest extends Unit
{
    public function test_saveForm()
    {
        $em = $this->prophesize(EntityManagerInterface::class);

        $model = new CmsLayoutWidgetModel($em->reveal());
        $wr = new ExampleWidgetsRegistry($model);

        $widget = new CmsLayoutWidget();

        $this->assertInstanceOf(KisphpEntityInterface::class, $wr->saveForm([
            'widget' => $widget,
            'content' => 'demo test',
        ]));
    }

    public function test_updateSubForm()
    {
        $em = Stub::makeEmpty(EntityManagerInterface::class);

        $model = new CmsLayoutWidgetModel($em);
        $wr = new ExampleWidgetsRegistry($model);

        $form = Stub::makeEmpty(FormBuilderInterface::class);
        $entity = Stub::makeEmpty(CmsLayoutWidget::class, [
            'getType' => 'TYPE_TEXT',
        ]);
        $container = Stub::makeEmpty(ContainerInterface::class);

        $this->assertNull($wr->updateSubForm($form, $entity, $container));
    }

    public function test_getForm_found()
    {
        $em = Stub::makeEmpty(EntityManagerInterface::class);

        $model = new CmsLayoutWidgetModel($em);
        $wr = new ExampleWidgetsRegistry($model);

        $this->assertNotEmpty($wr->getForm('widget.type.text'));
    }

    public function test_getForm_not_found()
    {
        $em = Stub::makeEmpty(EntityManagerInterface::class);

        $model = new CmsLayoutWidgetModel($em);
        $wr = new ExampleWidgetsRegistry($model);

        $this->expectException(\Exception::class);
        $wr->getForm('widget.type.not_found');
    }

    public function test_getWidgetsTypes()
    {
        $model = Stub::make(CmsLayoutWidgetModel::class);
        $widgetsRegistry = new ExampleWidgetsRegistry($model);

        $this->assertNotEmpty($widgetsRegistry->getWidgetTypes());
    }
}
