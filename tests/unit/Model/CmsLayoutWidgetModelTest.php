<?php

namespace tests\Model;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\QueryBuilder;
use Kisphp\CmsBundle\Entity\CmsLayoutColumn;
use Kisphp\CmsBundle\Entity\CmsLayoutWidget;
use Kisphp\CmsBundle\Model\CmsLayoutWidgetModel;
use Kisphp\CmsBundle\Repository\CmsLayoutWidgetRepository;
use Kisphp\Utils\Status;
use PHPUnit\Framework\TestCase;

/**
 * @group cmsa
 */
class CmsLayoutWidgetModelTest extends TestCase
{
    public function test_activate_models()
    {
        $model = $this->createModel();
        $activated = $model->activateWidgetsByInterval(new \DateTime('now'));

        $this->assertEquals(2, $activated);
    }

    public function test_deactivate_models()
    {
        $model = $this->createModel();
        $deactivated = $model->deactivateWidgetsByInterval(new \DateTime('now'));

        $this->assertEquals(3, $deactivated);
    }

    public function test_model_media_type()
    {
        $model = $this->createModel();

        $this->assertSame('widget', $model->getMediaType());
    }

    public function test_toArray()
    {
        $model = $this->createModel();

        $column = new CmsLayoutColumn();
        $column->setId(1);

        $widget = new CmsLayoutWidget();
        $widget->setId(1);
        $widget->setPosition(1);
        $widget->setIdColumn(1);
        $widget->setStatus(Status::ACTIVE);
        $widget->setTitle('title');
        $widget->setType('type');
        $widget->setContent('content');

        $this->assertSame([
            'id' => 1,
            'position' => 1,
            'id_column' => 1,
            'status' => Status::ACTIVE,
            'title' => 'title',
            'type' => 'type',
            'content' => 'content',
        ], $model->toArray($widget));
    }

    /**
     * @return \Kisphp\CmsBundle\Model\CmsLayoutWidgetModel
     * @throws \Kisphp\Exceptions\ConstantNotFound
     */
    protected function createModel()
    {
        $qb = \Mockery::mock(QueryBuilder::class);
        $qb->shouldReceive('select')->andReturnSelf();
        $qb->shouldReceive('from')->andReturnSelf();
        $qb->shouldReceive('where')->andReturnSelf();
        $qb->shouldReceive('andWhere')->andReturnSelf();
        $qb->shouldReceive('setParameter')->andReturnSelf();
        $qb->shouldReceive('getQuery')->andReturnSelf();
        $qb->shouldReceive('getResult')->andReturn($this->getLayoutWidgets());

        $em = \Mockery::mock(EntityManagerInterface::class);
        $em->shouldReceive('createQueryBuilder')->andReturn($qb);
        $em->shouldReceive('persist')->andReturnSelf();
        $em->shouldReceive('flush')->andReturnSelf();

        $metaData = \Mockery::mock(ClassMetadata::class);

        $repo = new CmsLayoutWidgetRepository($em, $metaData);

        $em->shouldReceive('getRepository')->andReturn($repo);

        return new CmsLayoutWidgetModel($em);
    }

    /**
     * @return array
     */
    protected function getLayoutWidgets()
    {
        // start one hour before but inactive
        $widget_should_be_activated_1 = new CmsLayoutWidget();
        $widget_should_be_activated_1->setDateStart(time() - 3600);
        $widget_should_be_activated_1->setStatus(Status::INACTIVE);

        // time interval in past but still active
        $widget_should_be_activated_2 = new CmsLayoutWidget();
        $widget_should_be_activated_2->setDateStart(time() - 3600);
        $widget_should_be_activated_2->setDateStop(time() + 3600);
        $widget_should_be_activated_2->setStatus(Status::INACTIVE);

        // time interval in past but still active
        $widget_should_be_deactivated_1 = new CmsLayoutWidget();
        $widget_should_be_deactivated_1->setDateStart(time() - 3900);
        $widget_should_be_deactivated_1->setDateStop(time() - 3600);
        $widget_should_be_deactivated_1->setStatus(Status::ACTIVE);

        // stop one hour before but active
        $widget_should_be_deactivated_2 = new CmsLayoutWidget();
        $widget_should_be_deactivated_2->setDateStop(time() - 3600);
        $widget_should_be_deactivated_2->setStatus(Status::ACTIVE);

        // start in one hour but active
        $widget_should_be_deactivated_3 = new CmsLayoutWidget();
        $widget_should_be_deactivated_3->setDateStart(time() + 3000);
        $widget_should_be_deactivated_3->setStatus(Status::ACTIVE);

        // do nothing on it
        $widget_should_be_skipped = new CmsLayoutWidget();

        return [
            $widget_should_be_activated_1,
            $widget_should_be_activated_2,
            $widget_should_be_deactivated_1,
            $widget_should_be_deactivated_2,
            $widget_should_be_deactivated_3,
            $widget_should_be_skipped,
        ];
    }
}
