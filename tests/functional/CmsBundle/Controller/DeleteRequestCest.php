<?php

namespace Tests\Functional\AdminBundle\Controller;

use Codeception\Example;

/**
 * @group delete
 */
class DeleteRequestCest
{
    /**
     * @param \FunctionalTester $i
     *
     * @dataProvider deleteUrlsProvider
     */
    public function delete_change(\FunctionalTester $i, Example $urls)
    {
        $i->sendAjaxPostRequest($urls['url'], [
            'id' => 1,
        ]);
        $i->canSeeResponseCodeIs(200);
    }

    /**
     * @return array
     */
    public function deleteUrlsProvider()
    {
        return [
            ['url' => '/cms/template/delete'],
            ['url' => '/cms/layout/delete/widget'],
            ['url' => '/cms/layout/delete'],
        ];
    }
}
