<?php

namespace Tests\Functional\AdminBundle\Controller;

/**
 * @group cms_template
 */
class CmsTemplatesControllerCest
{
    /**
     * @param \FunctionalTester $i
     */
    public function listing_page(\FunctionalTester $i)
    {
        $i->amOnPage('/cms/template');
        $i->see('Cms Templates', 'h3');
        $i->see('Title');
        $i->canSeeResponseCodeIs(200);
    }

    /**
     * @param \FunctionalTester $i
     */
    public function edit_page(\FunctionalTester $i)
    {
        $i->amOnPage('/cms/template');
        $i->see('Edit');
        $i->click('Edit');
        $i->see('Edit Cms Template');
        $i->canSeeResponseCodeIs(200);
    }

    /**
     * @param \FunctionalTester $i
     */
    public function add_page(\FunctionalTester $i)
    {
        $i->amOnPage('/cms/template');
        $i->see('Edit');
        $i->click('Create');
        $i->see('Create Cms Template');
        $i->fillField('cms_template_form[title]', 'Demo template');
        $i->fillField('cms_template_form[class]', 'demo');
        $i->fillField('cms_template_form[content]', '<div class="demo">demo</div>');
        $i->click('Save');
        $i->canSeeResponseCodeIs(200);
        $i->canSeeCurrentUrlMatches('/cms\/template\/edit/');
    }

    /**
     * @param \FunctionalTester $i
     */
    public function add_page_no_filling(\FunctionalTester $i)
    {
        $i->amOnPage('/cms/template');
        $i->see('Edit');
        $i->click('Create');
        $i->see('Create Cms Template');
        $i->click('Save');
        $i->canSeeResponseCodeIs(200);
        $i->canSee('This value should not be blank');
    }
}
