<?php

namespace Tests\Functional\AdminBundle\Controller;

use Kisphp\CmsBundle\DemoData\CmsDemo;
use Kisphp\FrameworkAdminBundle\Command\DemoDataCommand;

/**
 * @group cms
 */
class CmsControllerCest
{
    /**
     * @param \FunctionalTester $i
     */
    public function listing_page(\FunctionalTester $i)
    {
        $i->amOnPage('/cms/layout');
        $i->see('Cms Layout', 'h3');
        $i->see('Title');
        $i->canSeeResponseCodeIs(200);
    }

    /**
     * @param \FunctionalTester $i
     */
    public function edit_page(\FunctionalTester $i)
    {
        $i->amOnPage('/cms/layout');
        $i->see('Cms Layout', 'h3');
        $i->click('Edit');
        $i->see('Edit Cms Layout', 'h3');
        $i->see('Layout Settings');
        $i->canSeeResponseCodeIs(200);
        $i->fillField('title', 'Changed layout');
        $i->click('Save');
        $i->canSeeResponseCodeIs(200);
        $i->see('CMS Layout was successfully saved');
    }

    /**
     * @param \FunctionalTester $i
     */
    public function add_page(\FunctionalTester $i)
    {
        $i->amOnPage('/cms/layout');
        $i->see('Edit');
        $i->click('Create');
        $i->see('Create Cms Layout');
        $i->fillField('title', 'Demo Layout');
        $i->click('Save');
        $i->canSeeResponseCodeIs(200);
        $i->canSeeCurrentUrlMatches('/cms\/layout\/edit/');
    }

    /**
     * @param \FunctionalTester $i
     */
    public function add_page_no_filling(\FunctionalTester $i)
    {
        $i->amOnPage('/cms/layout');
        $i->see('Edit');
        $i->click('Create');
        $i->see('Create Cms Layout');
        $i->click('Save');
        $i->canSeeResponseCodeIs(200);
        $i->canSee('Please add a title for the layout');
    }

    /**
     * @param \FunctionalTester $i
     */
    public function edit_widget(\FunctionalTester $i)
    {
        $i->amOnPage('/cms/layout/widget/edit/1');
        $i->canSeeResponseCodeIs(200);
        $i->see('Title');
        $i->see('Type');
        $i->see('Date start');
        $i->see('Date stop');
        $i->see('Move to row/column');
        $i->fillField('cms_layout_container_form[widget][title]', 'New title for widget');
        $i->click('Save');
        $i->canSeeResponseCodeIs(200);
    }

    /**
     * @param \FunctionalTester $i
     */
    public function add_widget(\FunctionalTester $i)
    {
        $i->amOnPage('/cms/layout/widget/new/1');
        $i->canSeeResponseCodeIs(200);
        $i->see('Title');
        $i->see('Type');
        $i->see('Date start');
        $i->see('Date stop');
        $i->fillField('cms_layout_container_form[widget][title]', 'New title for widget');
        $i->click('Save');
        $i->canSeeResponseCodeIs(200);
    }

    /**
     * @param \FunctionalTester $i
     */
    public function add_widget_no_filled_data(\FunctionalTester $i)
    {
        $i->amOnPage('/cms/layout/widget/new/1');
        $i->canSeeResponseCodeIs(200);
        $i->see('Title');
        $i->see('Type');
        $i->see('Date start');
        $i->see('Date stop');
        $i->click('Save');
        $i->canSeeResponseCodeIs(200);
        $i->see('This value should not be blank');
    }

    /**
     * @dataProvider getWidgetsIds
     *
     * @param \FunctionalTester $i
     *
     * @group aaa
     */
    public function edit_each_widget(\FunctionalTester $i, \Codeception\Example $widget)
    {
        $i->amOnPage($widget['edit_url'] . $widget['id']);
        $i->canSeeResponseCodeIs(200);

        $i->see('Date start');
        $i->see('Change status automatically');
        $i->see('Edit Widget: ' . $widget['title']);

        $i->click('Save');
        $i->canSeeResponseCodeIs(200);
    }

    /**
     * @return array
     */
    protected function getWidgetsIds()
    {
        return CmsDemo::getWidgetDemoContent();
    }
}
