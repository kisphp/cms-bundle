<?php

namespace Tests\Functional\AdminBundle\Controller;

use Codeception\Example;

/**
 * @group status
 */
class StatusRequestCest
{
    /**
     * @param \FunctionalTester $i
     *
     * @dataProvider statusUrlsProvider
     */
    public function status_change(\FunctionalTester $i, Example $urls)
    {
        $i->sendAjaxPostRequest($urls['url'], [
            'id' => 1,
        ]);
        $i->canSeeResponseCodeIs(200);
    }

    /**
     * @return array
     */
    public function statusUrlsProvider()
    {
        return [
            ['url' => '/cms/template/status'],
            ['url' => '/cms/layout/widget/status'],
            ['url' => '/cms/layout/status'],
        ];
    }
}
