<?php

namespace Functional\MotoBundle\Controller;

use Codeception\Example;
use Kisphp\CmsBundle\DemoData\CmsDemo;

/**
 * @group cms
 */
class CmsWidgetsCest
{
    /**
     * @param \FunctionalTester $i
     * @param \Codeception\Example $urls
     *
     * @dataProvider urlsProvider
     */
    public function widget_edit_page(\FunctionalTester $i, Example $urls)
    {
        $i->sendAjaxPostRequest($urls['url'], [
            'id' => 1,
        ]);
        $i->canSeeResponseCodeIs(200);
    }

    protected function urlsProvider()
    {
        $widgetsTypes = CmsDemo::getWidgetDemoContent();

        $urls = [];
        foreach ($widgetsTypes as $type) {
            $urls[]['url'] = $type['edit_url'] . $type['id'];
        }

        return $urls;
    }
}
