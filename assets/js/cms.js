
let alr = new function(){
  let self = this;

  self.error = function(message){
    swal("Error", message, "error");
  };
};

let ajx = new function() {
  let self = this;

  /** if ajax url is null, the action will be in the same page */
  self.url = null;

  self.dataType = 'json';

  /**
   * @param newUrl
   * @returns {KisphpAjax}
   */
  self.setUrl = function(newUrl){
    self.url = newUrl;

    return self;
  };

  /**
   * @param newDataType
   * @returns {KisphpAjax}
   */
  self.setDataType = function(newDataType){
    self.dataType = newDataType;

    return self;
  };

  /**
   * makes Ajax call and then call a callback function with the response as parameter
   *
   * @param options
   * @param callbackFunction
   */
  self.ajaxSubmit = function(options, callbackFunction) {
    $.ajax({
      url: this.url,
      type: 'post',
      dataType: this.dataType,
      data: options
    })
      .done(function(response, textStatus, xhr){
        if (typeof callbackFunction === 'function') {
          return callbackFunction(response, xhr);
        } else if (typeof callbackFunction === 'string') {
          return call[callbackFunction](response, xhr);
        } else {
          return {ajaxResponse: response, xhr: xhr};
        }
      })
      .fail(function(response){
        if (parseInt(response.status) === 403) {
          alr.error('Permission denied');

          return null;
        }
      });
  };
};

let Form = new function(){
  let self = this;

  self.deleteRowUrl = '/cms/layout/delete/row';
  self.deleteColumnUrl = '/cms/layout/delete/column';

  self.loadData = (formData) => {
    let fd = formData;

    $('#form_page_title').val(fd.title);
    $('#form_page_url').val(fd.url);
    $('#form_id').val(fd.id);
    $('#form_page_seo_title').val(fd.seo_title);
    $('#form_page_seo_keywords').val(fd.seo_keywords);
    $('#form_page_seo_description').val(fd.seo_description);

    if (!fd.rows || fd.rows.length < 1) {
      return;
    }

    for (let rowIndex in fd.rows) {
      let row = fd.rows[rowIndex];

      self.createRow(row, false);

      for (let colIndex in row.columns) {
        let col = row.columns[colIndex];

        self.createColumn(row.id, col)
      }
    }
  }

  self.getRandomNumber = () => {
    let rnd = String(Math.random());

    return 'rand-' + rnd.replace('.', '');
  }

  self.createColumn = (rowIndex, column) => {
    let colTpl = $('#col-tpl')[0].innerHTML;
    let elementContainer = $('#columns-' + rowIndex);

    column = column || {
      id: '',
      id_parent: '',
      title: '',
      attr_class: ''
    };

    if (column.id === '' || typeof column.id === 'undefined') {
      column.id = self.getRandomNumber();
    }

    elementContainer.append(colTpl.formatString({
      row: rowIndex,
      col: column.id,
      title: column.title,
      id_parent: column.id_parent,
      attr_class: column.attr_class || '',
    }));
  };

  self.createRow = (row, addColumn = true) => {
    row = row || {};
    let elementContainer = $('#grid');
    let rowTpl = $('#row-tpl')[0].innerHTML;

    if (typeof row.title === 'undefined' || row.title === null || row.title === false) {
      row.title = 'New row';
    }

    if (row.id === '' || typeof row.id === 'undefined' || row.id === null || row.id === false) {
      row.id = self.getRandomNumber();
    }

    // populate template with form
    elementContainer.append(rowTpl.formatString(row));

    // preselect type
    $("#row-select-" + row.id).find('option').each(function(index, element){
      let elemVal = $(element).val();
      if (elemVal === row.type) {
        $(element).attr('selected', 'selected');
      }
    });

    // create columns
    if (addColumn){
      self.createColumn(row.id);
    }
  };

  self.removeRow = (id, callback) => {
    let options = {
      id: id
    };

    ajx.setUrl(self.deleteRowUrl).ajaxSubmit(options, callback);
  };

  self.removeColumn = (id, callback) => {
    let options = {
      id: id
    };
    ajx.setUrl(self.deleteColumnUrl).ajaxSubmit(options, callback);
  };

  self.sortWidgets = () => {
    $(".sortable").sortable({
      placeholder: "ui-state-highlight",
      update: function(event, ui){

        if (typeof sortActionUrl === 'undefined') {
          return '';
        }

        let ids = [];
        ui.item.parent().find('tr').each(function(index, el){
          ids.push($(el).attr('id').replace('row-', ''));
        });

        ajx.setUrl(sortActionUrl).ajaxSubmit({
          idsList: ids
        });
      }
    });
  },

  self.sortRows = () => {
    $( ".grid_sortable" ).sortable({
      forcePlaceholderSize: true,
      placeholder: ".grid_sortable",
      appendTo: '.grid_sortable',
      handle: '.panel-heading',
      tolerance: 'pointer',
      start: function(){
        $('.panel-body').addClass('hidden');
      },
      stop: function(){
        $('.panel-body').removeClass('hidden');
      },
      update: function(event, ui){

        // if (typeof sortActionUrl === 'undefined') {
        //   return '';
        // }

        console.log(ui);

        return;

        let ids = [];
        ui.item.parent().find('tr').each(function(index, el){
          ids.push($(el).attr('id').replace('row-', ''));
        });

        ajx.setUrl(sortActionUrl).ajaxSubmit({
          idsList: ids
        });
      }
    });
  }
};

module.exports = {
  init: () => {
    $('#row-add').click(function (e) {
      e.preventDefault();

      Form.createRow();
    });

    $(document).on('click', '.add-column', function (e) {
      e.preventDefault();

      let rowIndex = $(this).data('row-id');

      Form.createColumn(rowIndex);
    });

    $(document).on('click', '.remove-column', function (e) {
      e.preventDefault();

      let id = $(this).data('id');
      let elementContainer = $('#column-' + id);

      swal({
        title: trans.swal.remove.confirm.title,
        text: trans.swal.remove.confirm.text,
        icon: "warning",
        buttons: true,
        dangerMode: true
      })
        .then((willDelete) => {
          if (willDelete) {
            Form.removeColumn(id, function(resp){

              // allow delete column that was not saved yet
              if (id.toString().match(/rand/)) {
                resp.code = 200;
              }

              // delete column html if the remove from persistance was successfull
              if (resp.code !== 200) {
                return;
              }
              elementContainer.addClass('alert-danger has-error').fadeOut('slow', function(){
                elementContainer.remove();
              });
            });
          }
        });

    });

    $(document).on('click', '.remove-row', function (e) {
      e.preventDefault();

      let id = $(this).data('id');
      let elementContainer = $('#row-' + id);

      swal({
        title: trans.swal.remove.confirm.title,
        text: trans.swal.remove.confirm.text,
        icon: "warning",
        buttons: true,
        dangerMode: true,
      })
        .then((willDelete) => {
          if (willDelete) {
            Form.removeRow(id, function(resp){

              console.log(id);
              console.log(resp);

              console.log(id.toString().match(/rand/));

              // allow remove columns that were not saved yet
              if (id.toString().match(/rand/)) {
                resp.code == 200;
              }

              console.log(resp);

              if (resp.code !== 200) {
                return;
              }
              elementContainer.find('.panel-heading').addClass('alert-danger has-error');
              elementContainer.find('.panel-body').addClass('alert-danger has-error');
              elementContainer.addClass('alert-danger has-error').fadeOut('slow', function(){
                elementContainer.remove();
              });
            });
          }
        });

    });

    if (typeof formData !== 'undefined') {
      Form.loadData(formData);
    }
    Form.sortWidgets();
    // Form.sortRows();
  }
};
