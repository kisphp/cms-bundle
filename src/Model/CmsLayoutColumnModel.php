<?php

namespace Kisphp\CmsBundle\Model;

use Doctrine\ORM\EntityRepository;
use Kisphp\CmsBundle\Entity\CmsLayoutColumn;
use Kisphp\Entity\KisphpEntityInterface;
use Kisphp\FrameworkAdminBundle\Model\AbstractModel;

/**
 * @method EntityRepository getRepository()
 */
class CmsLayoutColumnModel extends AbstractModel
{
    const REPOSITORY = 'CmsBundle:CmsLayoutColumn';

    /**
     * @return \Kisphp\CmsBundle\Entity\CmsLayoutColumn|KisphpEntityInterface
     */
    public function createEntity()
    {
        return new CmsLayoutColumn();
    }
}
