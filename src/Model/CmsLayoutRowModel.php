<?php

namespace Kisphp\CmsBundle\Model;

use Doctrine\ORM\EntityRepository;
use Kisphp\CmsBundle\Entity\CmsLayoutRow;
use Kisphp\Entity\KisphpEntityInterface;
use Kisphp\FrameworkAdminBundle\Model\AbstractModel;

/**
 * @method EntityRepository getRepository()
 */
class CmsLayoutRowModel extends AbstractModel
{
    const REPOSITORY = 'CmsBundle:CmsLayoutRow';

    /**
     * @return CmsLayoutRow|KisphpEntityInterface
     */
    public function createEntity()
    {
        return new CmsLayoutRow();
    }
}
