<?php

namespace Kisphp\CmsBundle\Model;

use Kisphp\CmsBundle\Entity\CmsTemplate;
use Kisphp\Entity\KisphpEntityInterface;
use Kisphp\FrameworkAdminBundle\Model\AbstractModel;

/**
 * @method CmsTemplateRepository getRepository()
 */
class CmsTemplateModel extends AbstractModel
{
    const REPOSITORY = 'CmsBundle:CmsTemplate';

    /**
     * @return CmsTemplate|KisphpEntityInterface
     */
    public function createEntity()
    {
        return new CmsTemplate();
    }

    /**
     * @return CmsTemplate[]
     */
    public function getTemplatesFormEditor()
    {
        return $this->getRepository()
            ->findAll()
        ;
    }
}
