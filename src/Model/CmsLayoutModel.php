<?php

namespace Kisphp\CmsBundle\Model;

use Doctrine\ORM\EntityRepository;
use Kisphp\CmsBundle\Entity\CmsLayout;
use Kisphp\CmsBundle\Entity\CmsLayoutRow;
use Kisphp\Entity\KisphpEntityInterface;
use Kisphp\FrameworkAdminBundle\Model\AbstractModel;

/**
 * @method CmsLayoutRepository|EntityRepository getRepository()
 */
class CmsLayoutModel extends AbstractModel
{
    const REPOSITORY = 'CmsBundle:CmsLayout';

    const TITLE = 'title';
    const TYPE = 'type';
    const URL = 'url';
    const SEO_TITLE = 'seo_title';
    const SEO_KEYWORDS = 'seo_keywords';
    const SEO_DESCRIPTION = 'seo_description';
    const ROWS = 'rows';
    const ID = 'id';
    const ID_PARENT = 'id_parent';
    const ATTR_CLASS = 'attr_class';
    const CSS_CLASS = 'css_class';
    const CSS_ID = 'css_id';
    const COLUMNS = 'columns';
    const STATUS = 'status';

    /**
     * @param int $id
     *
     * @return mixed
     */
    public function getLayoutWithActiveElements($id)
    {
        return $this->getRepository()->getLayoutWithActiveElements($id);
    }

    /**
     * @throws \Kisphp\Exceptions\ConstantNotFound
     *
     * @return CmsLayoutColumnModel
     */
    public function getColumnModel()
    {
        return new CmsLayoutColumnModel($this->em);
    }

    /**
     * @return CmsLayoutRowModel
     */
    public function getRowModel()
    {
        return new CmsLayoutRowModel($this->em);
    }

    /**
     * @return CmsLayout
     */
    public function createEntity()
    {
        return new CmsLayout();
    }

    /**
     * @param array $formData
     *
     * @return bool|CmsLayout|KisphpEntityInterface
     */
    public function saveLayoutFromForm(array $formData)
    {
        /** @var CmsLayout $layout */
        $layout = $this->findOneOrCreateById($formData[static::ID]);

        if (empty($formData[static::TITLE])) {
            return false;
        }

        $layout->setTitle($formData[static::TITLE]);
        $layout->setUrl($formData[static::URL]);
        $layout->setSeoTitle($formData[static::SEO_TITLE]);
        $layout->setSeoKeywords($formData[static::SEO_KEYWORDS]);
        $layout->setSeoDescription($formData[static::SEO_DESCRIPTION]);

        if (empty($formData[static::ROWS]) === false) {
            foreach ($formData[static::ROWS] as $formRow) {
                $formRow[static::ID] = $this->forceIdType($formRow[static::ID]);

                $row = $this->getLayoutRowById($layout, $formRow[static::ID]);
                $row->setTitle($formRow[static::TITLE]);
                $row->setType($formRow[static::TYPE]);
                $row->setCssClass($formRow[static::CSS_CLASS]);
                $row->setCssId($formRow[static::CSS_ID]);
                $row->setIdParent($formRow[static::ID_PARENT]);

                if (empty($formRow[static::COLUMNS]) === false) {
                    foreach ($formRow[static::COLUMNS] as $formCol) {
                        $formCol[static::ID] = $this->forceIdType($formCol[static::ID]);

                        if (empty($formCol[static::ATTR_CLASS])) {
                            continue;
                        }

                        /** @var \Kisphp\CmsBundle\Entity\CmsLayoutColumn $column */
                        $column = $this->getLayoutColumnById($row, $formCol[static::ID]);
                        $column->setAttrClass($formCol[static::ATTR_CLASS]);
                        $column->setIdParent($formCol[static::ID_PARENT]);
                        $column->setTitle($formCol[static::TITLE]);
                    }
                }
            }
        }

        return $this->save($layout);
    }

    /**
     * @param CmsLayout $cmsLayout
     *
     * @return array
     */
    public function toArray(CmsLayout $cmsLayout)
    {
        $layout = [
            static::ID => $cmsLayout->getId(),
            static::STATUS => $cmsLayout->getStatus(),
            static::TITLE => $cmsLayout->getTitle(),
            static::URL => $cmsLayout->getUrl(),
            static::SEO_TITLE => $cmsLayout->getSeoTitle(),
            static::SEO_KEYWORDS => $cmsLayout->getSeoKeywords(),
            static::SEO_DESCRIPTION => $cmsLayout->getSeoDescription(),
        ];

        /** @var \Kisphp\CmsBundle\Entity\CmsLayoutRow $row */
        foreach ($cmsLayout->getRows() as $row) {
            $rowId = $row->getId();

            $layout[static::ROWS][$row->getId()] = [
                static::ID => $rowId,
                static::TITLE => $row->getTitle(),
                static::TYPE => $row->getType(),
                static::CSS_CLASS => $row->getCssClass(),
                static::CSS_ID => $row->getCssId(),
            ];
            /** @var \Kisphp\CmsBundle\Entity\CmsLayoutColumn $column */
            foreach ($row->getColumns() as $column) {
                $col = [
                    static::ID => $column->getId(),
                    static::ID_PARENT => $column->getIdParent(),
                    static::TITLE => $column->getTitle(),
                    static::ATTR_CLASS => $column->getAttrClass(),
                ];

                $layout[static::ROWS][$rowId][static::COLUMNS][$column->getId()] = $col;
            }
        }

        return $layout;
    }

    /**
     * @param \Kisphp\CmsBundle\Entity\CmsLayout $layout
     * @param $rowId
     *
     * @return \Kisphp\CmsBundle\Entity\CmsLayoutRow
     */
    protected function getLayoutRowById(CmsLayout $layout, $rowId)
    {
        $id = $this->forceIdType($rowId);

        /** @var \Kisphp\CmsBundle\Entity\CmsLayoutRow $row */
        foreach ($layout->getRows() as $row) {
            if ($row->getId() === $id) {
                return $row;
            }
        }

        $row = $this->getRowModel()->createEntity();
        $row->setLayout($layout);

        $layout->addRow($row);

        return $row;
    }

    /**
     * @param \Kisphp\CmsBundle\Entity\CmsLayoutRow $row
     * @param $columnId
     *
     * @return \Kisphp\CmsBundle\Model\CmsLayoutColumn|KisphpEntityInterface
     */
    protected function getLayoutColumnById(CmsLayoutRow $row, $columnId)
    {
        $id = $this->forceIdType($columnId);

        /** @var \Kisphp\CmsBundle\Entity\CmsLayoutColumn $col */
        foreach ($row->getColumns() as $col) {
            if ($col->getId() === $id) {
                return $col;
            }
        }

        $col = $this->getColumnModel()->createEntity();
        $col->setRow($row);

        $row->addColumn($col);

        return $col;
    }

    /**
     * @param int|string $id
     *
     * @return null|int
     */
    protected function forceIdType($id)
    {
        if (is_numeric($id) === false) {
            return 0;
        }

        return (int) $id;
    }
}
