<?php

namespace Kisphp\CmsBundle\Model;

use Doctrine\ORM\EntityRepository;
use Kisphp\CmsBundle\Entity\CmsLayoutWidget;
use Kisphp\CmsBundle\Repository\CmsLayoutWidgetRepository;
use Kisphp\Entity\KisphpEntityInterface;
use Kisphp\MediaBundle\Model\MediaModelInterface;
use Kisphp\Model\AbstractModel;
use Kisphp\Utils\Status;

/**
 * @method CmsLayoutWidgetRepository|EntityRepository getRepository()
 */
class CmsLayoutWidgetModel extends AbstractModel implements CmsModelInterface, MediaModelInterface
{
    const REPOSITORY = 'CmsBundle:CmsLayoutWidget';

    /**
     * @return string
     */
    public function getMediaType()
    {
        return 'widget';
    }

    /**
     * @param array $formData
     *
     * @return KisphpEntityInterface
     */
    public function saveWidgetForm(array $formData)
    {
        if (array_key_exists('content', $formData) === false) {
            $formData['content'] = [];
        }

        $widget = $formData['widget'];
        $widget->setContent(json_encode($formData['content']));

        return $this->save($widget);
    }

    /**
     * @param \DateTime $now
     *
     * @return int
     */
    public function activateWidgetsByInterval(\DateTime $now)
    {
        $widgets = $this->getRepository()->getInactiveWidgets();

        $activated = 0;
        foreach ($widgets as $widget) {
            $timeStart = $widget->getDateStart();
            $timeStop = $widget->getDateStop();

            if ($timeStart === null && $timeStop === null) {
                // no date selected and ignore
                continue;
            }

            if ($this->shouldBeActive($now, $timeStart, $timeStop)) {
                ++$activated;
                $widget->setStatus(Status::ACTIVE);
                //$widget->setChangeStatus(false);
                //echo 'enabling ' . $widget->getId() . PHP_EOL;
                $this->em->persist($widget);
            }
        }

        $this->em->flush();

        return $activated;
    }

    /**
     * @param \DateTime $now
     *
     * @return int
     */
    public function deactivateWidgetsByInterval(\DateTime $now)
    {
        $widgets = $this->getRepository()->getActiveWidgets();

        $deactivated = 0;
        foreach ($widgets as $widget) {
            /** @var null|int $timeStart */
            $timeStart = $widget->getDateStart();
            /** @var null|int $timeStop */
            $timeStop = $widget->getDateStop();

            if ($timeStart === null && $timeStop === null) {
                // no date selected and ignore
                continue;
            }

            if ($this->shouldBeActive($now, $timeStart, $timeStop) === false) {
                ++$deactivated;
                $widget->setStatus(Status::INACTIVE);
                //$widget->setChangeStatus(false);
                //echo 'disabling ' . $widget->getId() . PHP_EOL;
                $this->em->persist($widget);
            }
        }

        $this->em->flush();

        return $deactivated;
    }

    /**
     * @param int[] $ids
     */
    public function sortWidgets(array $ids)
    {
        /** @var CmsLayoutWidgetRepository $repo */
        $repo = $this->getRepository();
        $entities = $repo->findItemsInList($ids);
        $order = array_flip($ids);

        /** @var \Kisphp\CmsBundle\Entity\CmsLayoutWidget $widget */
        foreach ($entities as $widget) {
            $widget->setPosition($order[$widget->getId()]);
            $this->em->flush($widget);
        }

        $this->em->flush();
    }

    /**
     * @param int $id
     * @param mixed $column
     *
     * @return null|\Kisphp\CmsBundle\Entity\CmsLayoutWidget|KisphpEntityInterface|object
     */
    public function findWidgetForColumn($column, $id)
    {
        $entity = $this->find($id);
        if ($entity === null) {
            $entity = $this->createEntity();

            /** @var \Kisphp\CmsBundle\Entity\CmsLayoutColumn $column */
            $column = $this->createLayoutColumnModel()->find($column);

            $entity->setColumn($column);
        }

        return $entity;
    }

    /**
     * @param \Kisphp\CmsBundle\Entity\CmsLayoutWidget $widget
     *
     * @return array
     */
    public function toArray(CmsLayoutWidget $widget)
    {
        return [
            'id' => $widget->getId(),
            'position' => $widget->getPosition(),
            'id_column' => $widget->getIdColumn(),
            'status' => $widget->getStatus(),
            'title' => $widget->getTitle(),
            'type' => $widget->getType(),
            'content' => $widget->getContent(),
        ];
    }

    /**
     * @return \Kisphp\CmsBundle\Entity\CmsLayoutWidget|KisphpEntityInterface
     */
    public function createEntity()
    {
        return new CmsLayoutWidget();
    }

    /**
     * @param int $timeStart
     * @param int $timeStop
     *
     * @return bool
     */
    protected function shouldBeActive(\DateTime $now, $timeStart, $timeStop)
    {
        $currentTimestamp = $now->getTimestamp();

        if ($timeStart <= $currentTimestamp && $currentTimestamp <= $timeStop) {
            return true;
        }

        if ($timeStart <= $currentTimestamp && $timeStop === null) {
            return true;
        }

        return false;
    }

    /**
     * @return CmsLayoutColumnModel
     */
    protected function createLayoutColumnModel()
    {
        return new CmsLayoutColumnModel($this->em);
    }
}
