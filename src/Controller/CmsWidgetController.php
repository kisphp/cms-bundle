<?php

namespace Kisphp\CmsBundle\Controller;

use Kisphp\CmsBundle\Form\CmsLayoutContainerForm;
use Kisphp\Entity\KisphpEntityInterface;
use Kisphp\Entity\ToggleableInterface;
use Kisphp\Utils\Status;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class CmsWidgetController extends CmsController
{
    const SECTION_TITLE = 'section.title.cms.layout';
    const MODEL_NAME = 'model.cms.layout';
    const LISTING_TEMPLATE = '@Cms/Cms/index.html.twig';
    const EDIT_TEMPLATE = '@Cms/Cms/edit-layout.html.twig';

    const UPLOAD_TYPE = 'widget';

    /**
     * @var array
     */
    protected $section = [
        self::EDIT_PATH => 'adm_cms_layout_edit',
        self::LIST_PATH => 'adm_cms_layout',
        self::ADD_PATH => 'adm_cms_layout_add',
        self::STATUS_PATH => 'adm_cms_layout_status',
        self::DELETE_PATH => 'adm_cms_layout_delete',
    ];

    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function deleteAction(Request $request)
    {
        $id = $request->request->getInt('id');

        $model = $this->get('model.cms.layout.widget');

        /** @var KisphpEntityInterface $widget */
        $widget = $model->find($id);

        if ($widget === null) {
            return new JsonResponse([
                'code' => Response::HTTP_NOT_FOUND,
            ]);
        }

        $model->remove($widget);

        return new JsonResponse([
            'code' => Response::HTTP_OK,
            'objectId' => $id,
        ]);
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function statusAction(Request $request)
    {
        $id = $request->request->getInt('id');

        /** @var \Kisphp\CmsBundle\Model\CmsLayoutWidgetModel $model */
        $model = $this->get('model.cms.layout.widget');

        /** @var KisphpEntityInterface|ToggleableInterface $entity */
        $entity = $model->find($id);

        if ($entity === null) {
            return new JsonResponse([
                'code' => Response::HTTP_NOT_FOUND,
                'message' => $this->trans('alert.message.status.entity_not_found'),
            ]);
        }

        if ($entity->getStatus() === Status::INACTIVE) {
            $entity->setStatus(Status::ACTIVE);
        } else {
            $entity->setStatus(Status::INACTIVE);
        }
        $model->save($entity);

        return new JsonResponse([
            'code' => Response::HTTP_OK,
            'objectId' => $entity->getId(),
            'status' => $entity->getStatus(),
        ]);
    }

    /**
     * @param Request $request
     * @param int $column
     * @param int $id
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editAction(Request $request, $column = 0, $id = 0)
    {
        /** @var \Kisphp\CmsBundle\Entity\CmsLayoutWidget $entity */
        $entity = $this->get('model.cms.layout.widget')
            ->findWidgetForColumn($column, $id)
        ;

        $registry = $this->get('cms.widgets.registry');
        $container = $this->get('service_container');

        $defaultData = [
            'widget' => $entity,
            'content' => [],
        ];
        $form = $this->createForm(CmsLayoutContainerForm::class, $defaultData, [
            'container' => $container,
            'entity' => $entity,
            'widgets_registry' => $registry,
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();

            $entity = $registry->saveForm($data);

            $this->addFlash('success', 'CMS Widget was successfully saved');

            return $this->redirect(
                $this->generateUrl('adm_cms_layout_edit_widget', [
                    'id' => $entity->getId(),
                ])
            );
        }

        $layoutId = $entity->getColumn()->getRow()->getLayout();

        return $this->render('@Cms/Cms/edit-widget.html.twig', [
            'id' => $id,
            'form' => $form->createView(),
            'entity' => $entity,
            'section' => $this->section,
            'section_title' => ($id > 0) ? 'Edit Widget: ' . $entity->getTitle() : 'Add widget',
            'upload_type' => 'widget',
            'html_editor' => true,
            'images_list_url' => $this->getImagesEditUrl($id),
            'attached' => $this->getAttachedFiles($id),
            'templates' => $this->get('model.cms.template')->getTemplatesFormEditor(),
            'layout' => $this->get('model.cms.layout')->getLayoutWithActiveElements($layoutId),
        ]);
    }

    /**
     * @param \Symfony\Component\HttpFoundation\Request $request
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     *
     * @throws \Kisphp\Exceptions\ConstantNotFound
     */
    public function moveAction(Request $request)
    {
        $columnId = $request->request->getInt('column_id');
        $widgetId = $request->request->getInt('widget_id');

        $modelWidget = $this->get('model.cms.layout.widget');
        $modelColumn = $this->get('model.cms.layout')->getColumnModel();

        $redirectUrl = $this->generateUrl('adm_cms_layout_edit_widget', [
            'id' => $widgetId,
        ]);

        /** @var \Kisphp\CmsBundle\Entity\CmsLayoutWidget $widget */
        $widget = $modelWidget->find($widgetId);
        /** @var \Kisphp\CmsBundle\Entity\CmsLayoutColumn $column */
        $column = $modelColumn->find($columnId);

        if ($widget === null || $column === null) {
            $this->addFlash('error', 'Widget does not exists');

            return $this->redirect($redirectUrl);
        }

        $widget->setColumn($column);

        $modelWidget->save($widget);

        $this->addFlash('success', 'Widget successfully moved');

        return $this->redirect($redirectUrl);
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function sortAction(Request $request)
    {
        $ids = $request->request->get('idsList');

        $model = $this->get('model.cms.layout.widget');
        $model->sortWidgets($ids);

        return new JsonResponse([
            'core' => 200,
        ]);
    }
}
