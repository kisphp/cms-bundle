<?php

namespace Kisphp\CmsBundle\Controller;

use Kisphp\CmsBundle\Form\CmsTemplateForm;
use Kisphp\FrameworkAdminBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class CmsTemplateController extends AbstractController
{
    const SECTION_TITLE = 'section.title.cms.template';
    const MODEL_NAME = 'model.cms.template';
    const ENTITY_FORM_CLASS = CmsTemplateForm::class;
    const LISTING_TEMPLATE = '@Cms/CmsTemplate/index.html.twig';

    /**
     * @var array
     */
    protected $section = [
        self::EDIT_PATH => 'adm_cms_template_edit',
        self::LIST_PATH => 'adm_cms_template',
        self::ADD_PATH => 'adm_cms_template_add',
        self::STATUS_PATH => 'adm_cms_template_status',
        self::DELETE_PATH => 'adm_cms_template_delete',
    ];

    /**
     * @param int $id
     *
     * @return Response
     */
    public function viewAction($id)
    {
        /** @var \Kisphp\CmsBundle\Entity\CmsTemplate $entity */
        $entity = $this->get('model.cms.template')->find($id);

        return $this->render('@Cms/CmsTemplate/tpl.html.twig', [
            'entity' => $entity,
        ]);
    }
}
