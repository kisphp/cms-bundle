<?php

namespace Kisphp\CmsBundle\Controller;

use Kisphp\Entity\KisphpEntityInterface;
use Kisphp\FrameworkAdminBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class CmsController extends AbstractController
{
    const UPLOAD_TYPE = 'widget';

    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function deleteColumnAction(Request $request)
    {
        $id = $request->request->getInt('id');

        $columnModel = $this->get('model.cms.layout')->getColumnModel();
        /** @var KisphpEntityInterface $entity */
        $entity = $columnModel->find($id);

        if ($entity === null) {
            return new JsonResponse([
                'code' => Response::HTTP_NOT_FOUND,
            ]);
        }

        $columnModel->remove($entity);

        return new JsonResponse([
            'code' => Response::HTTP_OK,
            'objectId' => $id,
        ]);
    }

    /**
     * @param int $id
     *
     * @return KisphpEntityInterface
     */
    protected function createFormEntity($id)
    {
        $model = $this->get(static::MODEL_NAME);

        $object = $model->find($id);
        if (empty($object)) {
            $object = $model->createEntity();
        }

        return $object;
    }

    /**
     * @param int $id
     *
     * @return \Kisphp\CmsBundle\Entity\CmsLayout|\Kisphp\Entity\KisphpEntityInterface
     */
    protected function createEntityObject($id)
    {
        /** @var \Kisphp\CmsBundle\Entity\CmsLayout $object */
        $object = $this->createFormEntity($id);

        $layoutModel = $this->get('model.cms.layout');

        $rows = $object->getRows();
        if (count($rows) < 1) {
            $column = $layoutModel->getColumnModel()->createEntity();
            $row = $layoutModel->getRowModel()->createEntity();
            $column->setRow($row);
            $row->setLayout($object);
            $row->addColumn($column);
            $object->addRow($row);
        }

        return $object;
    }

    /**
     * @param int $id
     *
     * @return string
     */
    protected function getImagesEditUrl($id)
    {
        return $this->generateUrl('adm_cms_widget_attached', [
            'id' => $id,
        ]);
    }

    /**
     * @param int $articleId
     *
     * @return array
     */
    protected function getAttachedFiles($articleId)
    {
        $model = $this->get('model.media_files');

        return $model->findBy([
            'id_object' => $articleId,
            'object_type' => static::UPLOAD_TYPE,
        ]);
    }
}
