<?php

namespace Kisphp\CmsBundle\Controller;

use Kisphp\Entity\KisphpEntityInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class CmsRowController extends CmsController
{
    const SECTION_TITLE = 'section.title.cms.layout';
    const MODEL_NAME = 'model.cms.layout';
    const LISTING_TEMPLATE = '@Cms/Cms/index.html.twig';
    const EDIT_TEMPLATE = '@Cms/Cms/edit-layout.html.twig';

    /**
     * @var array
     */
    protected $section = [
        self::EDIT_PATH => 'adm_cms_layout_edit',
        self::LIST_PATH => 'adm_cms_layout',
        self::ADD_PATH => 'adm_cms_layout_add',
        self::STATUS_PATH => 'adm_cms_layout_status',
        self::DELETE_PATH => 'adm_cms_layout_delete',
    ];

    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function deleteAction(Request $request)
    {
        $id = $request->request->getInt('id');

        $rowModel = $this->get('model.cms.layout')->getRowModel();
        /** @var KisphpEntityInterface $entity */
        $entity = $rowModel->find($id);

        if ($entity === null) {
            return new JsonResponse([
                'code' => Response::HTTP_NOT_FOUND,
            ]);
        }

        $rowModel->remove($entity);

        return new JsonResponse([
            'code' => Response::HTTP_OK,
            'objectId' => $id,
        ]);
    }

    /**
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @param int $id
     */
    public function duplicateAction(Request $request, $id)
    {
        /** @var \Kisphp\CmsBundle\Entity\CmsLayout $object */
        $object = $this->createEntityObject($id);

        $layoutModel = $this->get('model.cms.layout');

        dump($object);
        die;
    }

    /**
     * @param int $id
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function viewAction($id)
    {
        /** @var \Kisphp\CmsBundle\Entity\CmsLayout $entity */
        $entity = $this->get('model.cms.layout')->getLayoutWithActiveElements($id);

        $urls = $this->section;
        $urls[self::DELETE_PATH] = 'adm_cms_layout_widget_delete';

        return $this->render('@Cms/Cms/view-layout.html.twig', [
            'entity' => $entity,
            'section' => $urls,
            'section_title' => 'Layout: ' . $entity->getTitle(),
        ]);
    }
}
