<?php

namespace Kisphp\CmsBundle\Controller;

use Symfony\Component\HttpFoundation\Request;

class CmsLayoutController extends CmsController
{
    const SECTION_TITLE = 'section.title.cms.layout';
    const MODEL_NAME = 'model.cms.layout';
    const LISTING_TEMPLATE = '@Cms/Cms/index.html.twig';
    const EDIT_TEMPLATE = '@Cms/Cms/edit-layout.html.twig';

    /**
     * @var array
     */
    protected $section = [
        self::EDIT_PATH => 'adm_cms_layout_edit',
        self::LIST_PATH => 'adm_cms_layout',
        self::ADD_PATH => 'adm_cms_layout_add',
        self::STATUS_PATH => 'adm_cms_layout_status',
        self::DELETE_PATH => 'adm_cms_layout_delete',
    ];

    public function duplicateAction(Request $request, $id)
    {
        /** @var \Kisphp\CmsBundle\Entity\CmsLayout $object */
        $object = $this->createEntityObject($id);

        $layoutModel = $this->get('model.cms.layout');

        dump($object);
        die;
    }

    /**
     * @param Request $request
     * @param int $id
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editAction(Request $request, $id = 0)
    {
        /** @var \Kisphp\CmsBundle\Entity\CmsLayout $object */
        $object = $this->createEntityObject($id);

        $layoutModel = $this->get('model.cms.layout');

        if ($request->isMethod(Request::METHOD_POST)) {
            $post = $request->request->all();

            $entity = $layoutModel->saveLayoutFromForm($post);

            if ($entity === false) {
                $this->addFlash('error', 'Please add a title for the layout');
            } else {
                $this->addFlash('success', 'CMS Layout was successfully saved');

                return $this->redirect(
                    $this->generateUrl('adm_cms_layout_edit', [
                        'id' => $entity->getId(),
                    ])
                );
            }
        }

        return $this->render(
            static::EDIT_TEMPLATE,
            [
                'entity' => $object,
                'formData' => $layoutModel->toArray($object),
                'section' => $this->section,
                'section_title' => static::SECTION_TITLE . (($id > 0) ? '.edit' : '.create'),
            ]
        );
    }

    /**
     * @param int $id
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function viewAction($id)
    {
        /** @var \Kisphp\CmsBundle\Entity\CmsLayout $entity */
        $entity = $this->get('model.cms.layout')->getLayoutWithActiveElements($id);

        $urls = $this->section;
        $urls[self::DELETE_PATH] = 'adm_cms_layout_widget_delete';

        return $this->render('@Cms/Cms/view-layout.html.twig', [
            'entity' => $entity,
            'section' => $urls,
            'section_title' => 'Layout: ' . $entity->getTitle(),
        ]);
    }
}
