<?php

namespace Kisphp\CmsBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Kisphp\CmsBundle\Entity\CmsLayoutColumn;
use Kisphp\CmsBundle\Entity\CmsLayoutRow;

class CmsLayoutRepository extends EntityRepository
{
    /**
     * @param int $id
     *
     * @return \Kisphp\CmsBundle\Entity\CmsLayout
     */
    public function getLayoutWithActiveElements($id)
    {
        $query = $this->createQueryBuilder('l')
            ->leftJoin(CmsLayoutRow::class, 'r', 'WITH', 'r.id_layout = l.id')
            ->leftJoin(CmsLayoutColumn::class, 'c', 'WITH', 'c.id_row = r.id')
            ->where('l.id = :id')
            ->setParameter('id', $id)
        ;

        return $query->getQuery()->getOneOrNullResult();
    }
}
