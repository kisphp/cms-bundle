<?php

namespace Kisphp\CmsBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Kisphp\Utils\Status;

class CmsLayoutWidgetRepository extends EntityRepository
{
    /**
     * @param array $ids
     *
     * @return \Kisphp\CmsBundle\Entity\CmsLayoutWidget[]
     */
    public function findItemsInList(array $ids)
    {
        $query = $this->createQueryBuilder('a')
            ->where('a.id IN (:ids)')
            ->setParameter('ids', $ids)
            ->andWhere('a.status > 0')
        ;

        return $query->getQuery()->getResult();
    }

    /**
     * @return \Kisphp\CmsBundle\Entity\CmsLayoutWidget[]
     */
    public function getInactiveWidgets()
    {
        $query = $this->createQueryBuilder('a')
            ->where('a.status = :status')
            ->setParameter('status', Status::INACTIVE)
            ->andWhere('a.change_status = 1')
        ;

        return $query->getQuery()->getResult();
    }

    /**
     * @return \Kisphp\CmsBundle\Entity\CmsLayoutWidget[]
     */
    public function getActiveWidgets()
    {
        $query = $this->createQueryBuilder('a')
            ->where('a.status = :status')
            ->setParameter('status', Status::ACTIVE)
            ->andWhere('a.change_status = 1')
        ;

        return $query->getQuery()->getResult();
    }
}
