<?php

namespace Kisphp\CmsBundle\FormWidgets;

use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;

class TextForm extends AbstractWidgetForm
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('text', TextareaType::class, [
            'attr' => [
                'class' => 'html-edit',
            ],
        ]);
    }
}
