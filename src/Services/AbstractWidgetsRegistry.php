<?php

namespace Kisphp\CmsBundle\Services;

use Kisphp\CmsBundle\FormWidgets\CodeForm;
use Kisphp\CmsBundle\FormWidgets\TextForm;
use Kisphp\CmsBundle\Model\CmsModelInterface;
use Kisphp\Entity\KisphpEntityInterface;
use Psr\Container\ContainerInterface;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\FormBuilderInterface;

abstract class AbstractWidgetsRegistry implements CmsModelInterface
{
    const FIELD_CONTENT = 'content';

    const TYPE_TEXT = 'TYPE_TEXT';
    const TYPE_CODE = 'TYPE_CODE';

    /**
     * @var \Kisphp\CmsBundle\Model\CmsModelInterface
     */
    protected $model;

    /**
     * @param \Kisphp\CmsBundle\Model\CmsModelInterface $model
     */
    public function __construct(CmsModelInterface $model)
    {
        $this->model = $model;
    }

    /**
     * @return array
     */
    public function getWidgetTypes()
    {
        $coreTypes = [
            'widget.type.text' => TextForm::class,
            'widget.type.code' => CodeForm::class,
        ];

        return array_merge($coreTypes, $this->getProjectWidgetTypes());
    }

    /**
     * @param array $formData
     *
     * @return KisphpEntityInterface
     */
    public function saveForm(array $formData)
    {
        return $this->model->saveWidgetForm($formData);
    }

    /**
     * @param $formName
     *
     * @return string
     *
     * @throws \Exception
     */
    public function getForm($formName)
    {
        $formTypes = $this->getWidgetTypes();

        if (isset($formTypes[$formName])) {
            return $formTypes[$formName];
        }

        throw new \Exception('Form not found');
    }

    /**
     * @param FormBuilderInterface $form
     * @param Cms|KisphpEntityInterface $entity
     */
    public function updateSubForm(FormBuilderInterface $form, KisphpEntityInterface $entity, ContainerInterface $container)
    {
        $type = $entity->getType();

        $formWidgetsTypes = $this->getWidgetTypes();
        if (isset($formWidgetsTypes[$type])) {
            $form->add(self::FIELD_CONTENT, $formWidgetsTypes[$type], [
                'container' => $container,
            ]);
            $form->addModelTransformer(new CallbackTransformer(
                function ($formData) use ($form) {
                    return $this->transformDataForm($formData, $form);
                },
                function ($formData) {
                    return $formData;
                }
            ));
        }
    }

    /**
     * @return array
     */
    abstract protected function getProjectWidgetTypes();

    /**
     * @param array $formData
     * @param FormBuilderInterface $form
     *
     * @return array
     */
    protected function transformDataForm($formData, $form)
    {
        $content = json_decode($formData['widget']->getContent(), true);

        /** @var FormBuilderInterface $widgetForm */
        $widgetForm = $form->get('content');

        foreach ($widgetForm->all() as $field) {
            if (isset($content[$field->getName()])) {
                $formData['content'][$field->getName()] = $content[$field->getName()];
            }
        }

        return $formData;
    }
}
