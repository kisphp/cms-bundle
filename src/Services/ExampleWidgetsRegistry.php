<?php

namespace Kisphp\CmsBundle\Services;

/**
 * @deprecated Don't use, this is only an example for tests
 */
class ExampleWidgetsRegistry extends AbstractWidgetsRegistry
{
    const TYPE_TITLE_TEXT = 'TYPE_TITLE_TEXT';

    /**
     * @return array
     */
    public function getProjectWidgetTypes()
    {
//        return [
//            'widget.type.title_text' => static::TYPE_TITLE_TEXT,
//        ];
        return [];
    }
}
