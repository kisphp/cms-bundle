<?php

namespace Kisphp\CmsBundle\Services\Cms;

use Kisphp\CmsBundle\Model\CmsLayoutWidgetModel;

class CmsStatusManager
{
    /**
     * @var \Kisphp\CmsBundle\Model\CmsLayoutWidgetModel
     */
    private $widgetModel;

    /**
     * @var string
     */
    private $websiteTimezone;

    /**
     * @param \Kisphp\CmsBundle\Model\CmsLayoutWidgetModel $widgetModel
     * @param string $websiteTimezone
     */
    public function __construct(CmsLayoutWidgetModel $widgetModel, $websiteTimezone)
    {
        $this->widgetModel = $widgetModel;
        $this->websiteTimezone = $websiteTimezone;
    }

    /**
     * @return int
     */
    public function activateWidgetsByInterval()
    {
        $now = new \DateTime('now', new \DateTimeZone($this->websiteTimezone));

        return $this->widgetModel->activateWidgetsByInterval($now);
    }

    /**
     * @return int
     */
    public function deactivateWidgetsByInterval()
    {
        $now = new \DateTime('now', new \DateTimeZone($this->websiteTimezone));

        return $this->widgetModel->deactivateWidgetsByInterval($now);
    }
}
