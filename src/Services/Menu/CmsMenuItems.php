<?php

namespace Kisphp\CmsBundle\Services\Menu;

use Kisphp\FrameworkAdminBundle\Services\MenuItemInterface;

class CmsMenuItems implements MenuItemInterface
{
    /**
     * @param \ArrayIterator $iterator
     */
    public function getMenuItems(\ArrayIterator $iterator)
    {
        $iterator->append([
            'is_header' => true,
            'valid_feature' => 'cms',
            'label' => 'CMS',
        ]);
        $iterator->append([
            'is_header' => false,
            'valid_feature' => 'cms',
            'match_route' => 'cms',
            'path' => 'adm_cms_layout',
            'icon' => 'fa-gem fa-diamond',
            'label' => 'main_navigation.cms',
        ]);
        $iterator->append([
            'is_header' => false,
            'valid_feature' => 'cms',
            'match_route' => 'template',
            'path' => 'adm_cms_template',
            'icon' => 'fa-window-restore',
            'label' => 'main_navigation.cms_templates',
        ]);
    }
}
