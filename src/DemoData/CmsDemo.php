<?php

namespace Kisphp\CmsBundle\DemoData;

use Kisphp\CmsBundle\Entity\CmsLayout;
use Kisphp\CmsBundle\Entity\CmsLayoutColumn;
use Kisphp\CmsBundle\Entity\CmsLayoutRow;
use Kisphp\CmsBundle\Entity\CmsLayoutWidget;
use Kisphp\FrameworkAdminBundle\Fixtures\AbstractDemoData;
use Kisphp\Utils\Status;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CmsDemo extends AbstractDemoData
{
    /**
     * @param \Symfony\Component\Console\Input\InputInterface $input
     * @param \Symfony\Component\Console\Output\OutputInterface $output
     */
    public function loadDemoData(InputInterface $input, OutputInterface $output)
    {
        $this->createHomepageLayout($output);
        $this->createCmsTemplate($output);
        $this->populateDemoLayout($output);
    }

    /**
     * @return array
     */
    public static function getWidgetDemoContent()
    {
        return [
            [
                'id' => 1,
                'title' => 'Type Text',
                'edit_url' => '/cms/layout/widget/edit/',
                'type' => 'widget.type.text',
                'data' => [
                    'text' => 'demo text widget content',
                ],
            ],
            [
                'id' => 2,
                'title' => 'Type Code',
                'edit_url' => '/cms/layout/widget/edit/',
                'type' => 'widget.type.code',
                'data' => [
                    'code' => '<script>console.log("demo code");</script>',
                ],
            ],
        ];
    }

    /**
     * @param OutputInterface $output
     */
    protected function createCmsTemplate(OutputInterface $output)
    {
        $model = $this->getContainer()->get('model.cms.template');

        $entity = $model->createEntity();
        $entity->setTitle('Cms template');
        $entity->setContent('<div class="alert alert-info">alert 1</div>');

        $model->save($entity);

        $entity = $model->createEntity();
        $entity->setTitle('Cms template');
        $entity->setContent('<div class="alert alert-info">alert 2</div>');
        $entity->setStatus(Status::INACTIVE);

        $model->save($entity);

        $output->writeln('<info>Created CMS Template</info>');
    }

    /**
     * @param OutputInterface $output
     */
    protected function createHomepageLayout(OutputInterface $output)
    {
        $layoutModel = $this->getContainer()->get('model.cms.layout');

        $layout = new CmsLayout();
        $layout->setTitle('Demo Layout Cms Bundle');
        $layout->setUrl('/demo-layout');
        $layout->setSeoTitle('demo seo title');
        $layout->setSeoKeywords('demo seo keywords');
        $layout->setSeoDescription('demo seo description');

        $row = new CmsLayoutRow();
        $row->setCssClass('css_class');
        $row->setCssId('css_id');
        $row->setTitle('Row CMS 1');
        $row->setLayout($layout);

        $layout->addRow($row);

        $column = new CmsLayoutColumn();
        $column->setRow($row);
        $column->setAttrClass('col-xs-12');
        $row->addColumn($column);

        $layoutModel->save($layout);

        $output->writeln('<info>Added Demo CMS Layout</info>');
    }

    /**
     * @param OutputInterface $output
     */
    protected function populateDemoLayout(OutputInterface $output)
    {
        $cmsModel = $this->getContainer()->get('model.cms.layout');
        $em = $this->getContainer()->get('doctrine.orm.default_entity_manager');

        /** @var CmsLayoutRow $row */
        $row = $cmsModel->getRowModel()->find(1);

        foreach (self::getWidgetDemoContent() as $widgetData) {
            $widget = new CmsLayoutWidget();
            $widget->setId($widgetData['id']);
            $widget->setTitle($widgetData['title']);
            $widget->setType($widgetData['type']);
            $widget->setColumn($row->getColumns()[0]);
            $widget->setContent(json_encode($widgetData['data']));
            $em->persist($widget);
        }

        $em->flush();

        $output->writeln('<info>Populated Demo CMS Layout</info>');
    }
}
