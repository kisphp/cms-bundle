<?php

namespace Kisphp\CmsBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Kisphp\Entity\KisphpEntityInterface;
use Kisphp\Entity\ToggleableInterface;
use Kisphp\Utils\Status;

/**
 * @ORM\Entity
 * @ORM\Table(name="cms_layout_row", options={"colate": "utf8_general_ci", "charset": "utf8"})
 */
class CmsLayoutRow implements KisphpEntityInterface, ToggleableInterface
{
    /**
     * @var int
     *
     * @ORM\Column(type="integer", options={"unsigned": true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", nullable=true, options={"unsigned": true})
     */
    protected $id_parent;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", nullable=true, options={"default": 1000})
     */
    protected $position;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", options={"default": Kisphp\Utils\Status::ACTIVE})
     */
    protected $status = Status::ACTIVE;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", options={"unsigned": true})
     */
    protected $id_layout;

    /**
     * @ORM\ManyToOne(targetEntity="CmsLayout", inversedBy="rows")
     * @ORM\JoinColumn(name="id_layout", referencedColumnName="id")
     */
    protected $layout;

    /**
     * @ORM\ManyToOne(targetEntity="CmsLayoutRow")
     * @ORM\JoinColumn(name="id_parent", referencedColumnName="id")
     */
    protected $parent;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, options={"default": ""})
     */
    protected $title = '';

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=64, options={"default": ""})
     */
    protected $cssClass = '';

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=64, options={"default": ""})
     */
    protected $cssId = '';

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=128, options={"default": ""})
     */
    protected $type = '';

    /**
     * @ORM\OneToMany(targetEntity="CmsLayoutColumn", mappedBy="row", cascade={"persist", "remove"})
     */
    protected $columns;

    public function __construct()
    {
        $this->columns = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return 'CMS Row: ' . $this->getId();
    }

    /**
     * @return int
     */
    public function getIdParent(): int
    {
        return $this->id_parent;
    }

    /**
     * @param int $id_parent
     */
    public function setIdParent($id_parent)
    {
        $idParent = (int) $id_parent;

        if (!is_numeric($idParent) || $idParent < 1) {
            $idParent = null;
        }

        $this->id_parent = $idParent;
    }

    /**
     * @return mixed
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @param mixed $parent
     */
    public function setParent($parent): void
    {
        $this->parent = $parent;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param int $status
     *
     * @return $this
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = (int) $id;
    }

    /**
     * @return int
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * @param int $position
     */
    public function setPosition($position)
    {
        $this->position = $position;
    }

    /**
     * @return mixed
     */
    public function getIdLayout()
    {
        return $this->id_layout;
    }

    /**
     * @param mixed $id_layout
     */
    public function setIdLayout($id_layout)
    {
        $this->id_layout = $id_layout;
    }

    /**
     * @return mixed
     */
    public function getLayout()
    {
        return $this->layout;
    }

    /**
     * @param mixed $layout
     */
    public function setLayout(CmsLayout $layout)
    {
        $this->layout = $layout;
        $this->id_layout = $layout->getId();
    }

    /**
     * @return mixed
     */
    public function getColumns()
    {
        return $this->columns;
    }

    /**
     * @param mixed $columns
     */
    public function setColumns($columns)
    {
        $this->columns = $columns;
    }

    /**
     * @param CmsLayoutColumn $column
     */
    public function addColumn(CmsLayoutColumn $column)
    {
        $this->columns->add($column);
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getCssClass()
    {
        return $this->cssClass;
    }

    /**
     * @param string $cssClass
     */
    public function setCssClass($cssClass)
    {
        $this->cssClass = $cssClass;
    }

    /**
     * @return string
     */
    public function getCssId()
    {
        return $this->cssId;
    }

    /**
     * @param string $cssId
     */
    public function setCssId($cssId)
    {
        $this->cssId = $cssId;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }
}
