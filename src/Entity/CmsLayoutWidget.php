<?php

namespace Kisphp\CmsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Kisphp\Entity\KisphpEntityInterface;
use Kisphp\Entity\ToggleableInterface;
use Kisphp\Utils\Status;

/**
 * @ORM\Table(name="cms_layout_widgets", options={"colate": "utf8_general_ci", "charset": "utf8"})
 * @ORM\Entity(repositoryClass="Kisphp\CmsBundle\Repository\CmsLayoutWidgetRepository")
 */
class CmsLayoutWidget implements KisphpEntityInterface, ToggleableInterface
{
    /**
     * @var int
     *
     * @ORM\Column(type="integer", options={"unsigned": true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", options={"default": Kisphp\Utils\Status::ACTIVE})
     */
    protected $status = Status::ACTIVE;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean", options={"default": false})
     */
    protected $change_status = false;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", options={"default": 1000})
     */
    protected $position = 1000;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", options={"unsigned": true})
     */
    protected $id_column;

    /**
     * @ORM\ManyToOne(targetEntity="CmsLayoutColumn", inversedBy="widgets")
     * @ORM\JoinColumn(name="id_column", referencedColumnName="id")
     */
    protected $column;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $title;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $type;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", nullable=true, options={"unsigned": true, "default": NULL})
     */
    protected $date_start;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", nullable=true, options={"unsigned": true, "default": NULL})
     */
    protected $date_stop;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    protected $content;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param int $status
     *
     * @return $this
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return int
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * @param int $position
     */
    public function setPosition($position)
    {
        $this->position = $position;
    }

    /**
     * @return int
     */
    public function getIdColumn()
    {
        return $this->id_column;
    }

    /**
     * @param int $id_column
     */
    public function setIdColumn($id_column)
    {
        $this->id_column = $id_column;
    }

    /**
     * @return CmsLayoutColumn
     */
    public function getColumn()
    {
        return $this->column;
    }

    /**
     * @param CmsLayoutColumn $column
     */
    public function setColumn(CmsLayoutColumn $column)
    {
        $this->column = $column;
        $this->id_column = $column->getId();
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param string $content
     */
    public function setContent($content)
    {
        $this->content = $content;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return int
     */
    public function getDateStart()
    {
        return $this->date_start;
    }

    /**
     * @param int $date_start
     */
    public function setDateStart($date_start)
    {
        $this->date_start = $date_start;
    }

    /**
     * @return int
     */
    public function getDateStop()
    {
        return $this->date_stop;
    }

    /**
     * @param int $date_stop
     */
    public function setDateStop($date_stop)
    {
        $this->date_stop = $date_stop;
    }

    /**
     * @return bool
     */
    public function isChangeStatus()
    {
        return $this->change_status;
    }

    /**
     * @param bool $change_status
     */
    public function setChangeStatus($change_status)
    {
        $this->change_status = (bool) $change_status;
    }
}
