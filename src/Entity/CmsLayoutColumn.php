<?php

namespace Kisphp\CmsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Kisphp\Entity\KisphpEntityInterface;

/**
 * @ORM\Entity
 * @ORM\Table(name="cms_layout_columns", options={"colate": "utf8_general_ci", "charset": "utf8"})
 */
class CmsLayoutColumn implements KisphpEntityInterface
{
    /**
     * @var int
     *
     * @ORM\Column(type="integer", options={"unsigned": true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", options={"unsigned": true})
     */
    protected $id_row;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", nullable=true, options={"unsigned": true})
     */
    protected $id_parent;

    /**
     * @ORM\ManyToOne(targetEntity="CmsLayoutRow", inversedBy="columns")
     * @ORM\JoinColumn(name="id_row", referencedColumnName="id")
     */
    protected $row;

    /**
     * @var CmsLayoutWidget
     *
     * @ORM\OneToMany(targetEntity="CmsLayoutWidget", mappedBy="column", cascade={"remove"})
     * @ORM\OrderBy({"position" = "ASC"})
     */
    protected $widgets;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, options={"default": ""})
     */
    protected $attr_class = '';

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, options={"default": ""})
     */
    protected $title = '';

    /**
     * @return array
     */
    public function toArray()
    {
        return [
            'id' => $this->getId(),
            'id_parent' => $this->getIdParent(),
            'id_row' => $this->getIdRow(),
            'class' => $this->getAttrClass(),
            'title' => $this->getTitle(),
        ];
    }

    /**
     * @return int
     */
    public function getIdParent()
    {
        return $this->id_parent;
    }

    /**
     * @param int $id_parent
     */
    public function setIdParent($id_parent)
    {
        if ((int) $id_parent < 1) {
            $id_parent = null;
        }

        $this->id_parent = $id_parent;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getAttrClass()
    {
        return $this->attr_class;
    }

    /**
     * @param string $attr_class
     */
    public function setAttrClass($attr_class)
    {
        $this->attr_class = trim($attr_class);
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = (int) $id;
    }

    /**
     * @return mixed
     */
    public function getIdRow()
    {
        return $this->id_row;
    }

    /**
     * @param mixed $id_row
     */
    public function setIdRow($id_row)
    {
        $this->id_row = $id_row;
    }

    /**
     * @return mixed
     */
    public function getRow()
    {
        return $this->row;
    }

    /**
     * @param mixed $row
     */
    public function setRow($row)
    {
        $this->row = $row;
    }

    /**
     * @return mixed
     */
    public function getWidgets()
    {
        return $this->widgets;
    }

    /**
     * @param mixed $widgets
     */
    public function setWidgets($widgets)
    {
        $this->widgets = $widgets;
    }
}
