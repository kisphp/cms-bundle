<?php

namespace Kisphp\CmsBundle\Form;

use Kisphp\CmsBundle\Entity\CmsTemplate;
use Kisphp\Utils\Strings;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class CmsTemplateForm extends AbstractType
{
    /**
     * @param \Symfony\Component\OptionsResolver\OptionsResolver $resolver
     *
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'attr' => [
                'novalidate' => 'novalidate',
            ],
        ]);
    }

    /**
     * @param \Symfony\Component\Form\FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('title', TextType::class, [
            'constraints' => [
                new NotBlank(),
            ],
        ]);
        $builder->add('class', TextType::class, [
            'label' => 'Container extra class',
        ]);
        $builder->add('content', TextareaType::class, [
            'label' => 'HTML code',
            'constraints' => [
                new NotBlank(),
            ],
            'attr' => [
                'rows' => 12,
            ],
        ]);

        $builder->addModelTransformer(new CallbackTransformer(
            function ($entity) {
                return $entity;
            },
            function ($entity) use ($options) {
                return $this->transformEntity($entity, $options);
            }
        ));
    }

    /**
     * @param \Kisphp\CmsBundle\Entity\CmsTemplate $entity
     * @param array $options
     *
     * @return \Kisphp\CmsBundle\Entity\CmsTemplate
     */
    protected function transformEntity(CmsTemplate $entity, array $options)
    {
        if ($entity->getClass() === null) {
            $entity->setClass(Strings::niceUrlTitle($entity->getTitle()));
        }

        return $entity;
    }
}
