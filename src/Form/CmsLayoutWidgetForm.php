<?php

namespace Kisphp\CmsBundle\Form;

use Kisphp\CmsBundle\Entity\CmsLayoutWidget;
use Kisphp\Utils\Status;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Required;

class CmsLayoutWidgetForm extends AbstractType
{
    /**
     * @param OptionsResolver $resolver
     *
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'attr' => [
                'novalidate' => 'novalidate',
            ],
            'data_class' => CmsLayoutWidget::class,
        ]);
        $resolver->setRequired('container');
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('title', TextType::class, [
            'label' => 'form.general.title',
            'constraints' => [
                new NotBlank(),
                new Required(),
            ],
        ]);
        $builder->add('status', ChoiceType::class, [
            'label' => 'form.general.status',
            'expanded' => true,
            'choices' => [
                'status.active' => Status::ACTIVE,
                'status.inactive' => Status::INACTIVE,
            ],
            'attr' => [
                'class' => 'input-choice',
            ],
        ]);
        $cmsWidgets = array_keys($options['container']->get('cms.widgets.registry')->getWidgetTypes());
        $builder->add('type', ChoiceType::class, [
            'label' => 'form.general.type',
            'choices' => array_combine($cmsWidgets, $cmsWidgets),
        ]);
        $builder->add('date_start', TextType::class);
        $builder->add('date_stop', TextType::class);

        $builder->add('change_status', CheckboxType::class, [
            'label' => 'form.cms.automatic_status_change',
        ]);

        $builder->addModelTransformer(new CallbackTransformer(
            function ($entity) {
                $timeStart = $this->createDateTimeFromSeconds($entity->getDateStart());
                if ($timeStart !== null) {
                    $entity->setDateStart($timeStart->format('Y-m-d'));
                }

                $timeStop = $this->createDateTimeFromSeconds($entity->getDateStop());
                if ($timeStop !== null) {
                    $entity->setDateStop($timeStop->format('Y-m-d'));
                }

                return $entity;
            },
            function ($entity) use ($options) {
                return $this->transformEntity($entity, $options);
            }
        ));
    }

    /**
     * @param \Kisphp\CmsBundle\Entity\CmsLayoutWidget $widget
     *
     * @return \Kisphp\CmsBundle\Entity\CmsLayoutWidget
     */
    protected function transformEntity(CmsLayoutWidget $widget)
    {
        if ($widget->getDateStart() !== null) {
            $timeStart = $this->createDateTimestamp($widget->getDateStart(), '00:00:00');
            $widget->setDateStart($timeStart->getTimestamp());
        }

        if ($widget->getDateStop() !== null) {
            $timeStop = $this->createDateTimestamp($widget->getDateStop(), '23:59:59');
            $widget->setDateStop($timeStop->getTimestamp());
        }

        return $widget;
    }

    /**
     * @param string $dateString
     * @param mixed $dayPeriod
     *
     * @return bool|\DateTime
     */
    protected function createDateTimestamp($dateString, $dayPeriod)
    {
        return \DateTime::createFromFormat(
            'Y-m-d G:i:s',
            $dateString . ' ' . $dayPeriod
        );
    }

    /**
     * @param int $seconds
     *
     * @return null|\DateTime
     */
    protected function createDateTimeFromSeconds($seconds)
    {
        if ($seconds === null) {
            return null;
        }

        $timeStart = new \DateTime('now');
        $timeStart->setTimestamp($seconds);

        return $timeStart;
    }
}
