<?php

namespace Kisphp\CmsBundle\Form;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CmsLayoutContainerForm extends AbstractType
{
    /**
     * @param OptionsResolver $resolver
     *
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'attr' => [
                'novalidate' => 'novalidate',
            ],
        ]);

        $resolver->setRequired('container');
        $resolver->setRequired('entity');
        $resolver->setRequired('widgets_registry');
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var ContainerInterface $container */
        $container = $options['container'];
        /** @var \Kisphp\CmsBundle\Entity\CmsLayoutWidget $entity */
        $entity = $options['entity'];
        /** @var \Kisphp\CmsBundle\Services\AbstractWidgetsRegistry $registry */
        $registry = $options['widgets_registry'];

        $builder->add('widget', CmsLayoutWidgetForm::class, [
            'container' => $container,
        ]);

        $registry->updateSubForm($builder, $entity, $container);
    }
}
