<?php

namespace Kisphp\CmsBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ActivateWidgetsCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('cms:widget:activate')
            ->setDescription('Activate widgets if they match the current time interval')
        ;
    }

    /**
     * @param \Symfony\Component\Console\Input\InputInterface $input
     * @param \Symfony\Component\Console\Output\OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $model = $this->getContainer()->get('cms.status.manager');

        $count = $model->activateWidgetsByInterval();

        $output->writeln(sprintf('Activated %d widgets', $count));
    }
}
