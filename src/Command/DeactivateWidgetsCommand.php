<?php

namespace Kisphp\CmsBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class DeactivateWidgetsCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('cms:widget:deactivate')
            ->setDescription('Deactivate widgets if they match the current time interval')
        ;
    }

    /**
     * @param \Symfony\Component\Console\Input\InputInterface $input
     * @param \Symfony\Component\Console\Output\OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $model = $this->getContainer()->get('cms.status.manager');

        $count = $model->deactivateWidgetsByInterval();

        $output->writeln(sprintf('Deactivated %d widgets', $count));
    }
}
