<?php

namespace Kisphp\CmsBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CmsFixWidgetsTypeCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('cms:fix:type')
            ->setDescription('Fix CMS Widgets types')
        ;
    }

    /**
     * @param \Symfony\Component\Console\Input\InputInterface $input
     * @param \Symfony\Component\Console\Output\OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $repo = $this->getContainer()->get('doctrine')->getRepository('CmsBundle:CmsLayoutWidget');

        $em = $this->getContainer()->get('doctrine.orm.entity_manager');

        $widgets = $repo->findAll();

        foreach ($widgets as $widget) {
            $widget->setType($this->convertOldWidgetType($widget->getType()));

            $em->persist($widget);
        }

        $em->flush();

        $output->writeln('<info>Converted widgets types</info>');
    }

    /**
     * @param string $oldType
     *
     * @return string
     */
    protected function convertOldWidgetType($oldType)
    {
        $newType = str_replace('type_', '', strtolower($oldType));

        return 'widget.type.' . $newType;
    }
}
