<?php

namespace Kisphp\CmsBundle\Command;

use Kisphp\CmsBundle\Model\CmsLayoutColumnModel;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CmsUpdateColumnsCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('cms:update:columns')
            ->setDescription('Update columns class definition')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine')->getManager();
        $repo = $this->getContainer()->get('doctrine')->getRepository(CmsLayoutColumnModel::REPOSITORY);

        $columns = $repo->findAll();

        /** @var \Kisphp\CmsBundle\Entity\CmsLayoutColumn $column */
        foreach ($columns as $column) {
            $class = explode(' ', $column->getAttrClass());
            if (!empty($column->getXs())) {
                $class[] = 'col-xs-' . $column->getXs();
            }
            if (!empty($column->getSm())) {
                $class[] = 'col-sm-' . $column->getSm();
            }
            if (!empty($column->getMd())) {
                $class[] = 'col-md-' . $column->getMd();
            }
            if (!empty($column->getLg())) {
                $class[] = 'col-lg-' . $column->getLg();
            }

            $column->setAttrClass(implode(' ', $class));

            $em->persist($column);
        }

        $em->flush();

        $output->writeln('<info>Updated commands</info>');
    }
}
